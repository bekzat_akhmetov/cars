package com.company;

import com.company.domain.SUVs;
import com.company.domain.Sedans;
import com.company.domain.SuperCars;
import com.company.interfaces.ICars;

public class CarsFactory {
    public ICars getTypes(String carTypes) {
        if (carTypes == null) {
            return null;
        }

        if (carTypes.equalsIgnoreCase("Sedans")) {
            return new Sedans();
        }

        else if (carTypes.equalsIgnoreCase("SuperCars")) {
            return new SuperCars();
        }

        else if (carTypes.equalsIgnoreCase("SUVs")) {
            return new SUVs();
        }

        return null;
    }
}
