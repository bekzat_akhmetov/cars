package com.company;

public class Cars {
    private String name;
    private String color;
    private String cylinder;


    public Cars(CarsBuilder beka) {
        this.name=beka.name;
        this.color=beka.color;
        this.cylinder=beka.cylinder;
    }

    public Cars(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCylinder() {
        return cylinder;
    }

    public void setCylinder(String cylinder) {
        this.cylinder = cylinder;
    }

    public static class CarsBuilder{

        private String name;
        private String color;
        private String cylinder;

        public CarsBuilder(String name, String color, String cylinder) {
            this.name = name;
            this.color = color;
            this.cylinder = cylinder;
        }

        public CarsBuilder withName(String name){
            this.name=name;
            return this;
        }

        public CarsBuilder withColor(String color){
            this.color=color;
            return this;
        }
        public Cars build(){
            return new Cars(this);
        }
    }
}
