package com.company.domain;

import com.company.Cars;
import com.company.interfaces.ICars;

public class Sedans extends Cars implements ICars {
    public Sedans(){
        super("Sedan");
    }

    @Override
    public void engine() {
        System.out.println("The engine is 2L");
    }

    @Override
    public String toString() {
        return "Mercedes, BMW";
    }
}
