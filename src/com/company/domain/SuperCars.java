package com.company.domain;

import com.company.Cars;
import com.company.interfaces.ICars;

public class SuperCars extends Cars implements ICars {

    public SuperCars(){
        super("Supercar");
    }

    @Override
    public void engine() {
        System.out.println("The engine is 6L");
    }

    @Override
    public String toString() {
        return "Ferrari, Lamborghini, Porsche";
    }
}
