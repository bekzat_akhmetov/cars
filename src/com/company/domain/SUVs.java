package com.company.domain;

import com.company.Cars;
import com.company.interfaces.ICars;

public class SUVs extends Cars implements ICars {

    public SUVs() {
        super("Audi Q3");
    }

    @Override
    public void engine() {
        System.out.println("The engine is 8L");
    }

    @Override
    public String toString() {
        return "Audi Q3, Chevrolet Trailblazer";
    }
}
